stages:
  - build
  - pack
  - deploy

variables:
  # Avoid shallow clone, as otherwise the total number of commits for versioning cannot be determined.
  GIT_DEPTH: 0
  # Path to project sources, relative to the repository root. Can contain globs. Must not be empty.
  SOURCES_DIR: '.'
  # Directory for build artifacts, relative to the source.
  OUTPUTS_DIR: 'bin'
  # Directory used for build and restore objects, relative to the source.
  OBJECTS_DIR: 'obj'
  # Cache directory for NuGet packages, relative to project root.
  NUGET_PACKAGES_DIR: '.nuget'
  # Path to NuGet packages cache (used implicitly by NuGet and must be absolute).
  NUGET_PACKAGES: '$CI_PROJECT_DIR/$NUGET_PACKAGES_DIR'

  BUILD_CONFIGURATION: 'Debug'

default:
  image: mcr.microsoft.com/dotnet/sdk:6.0
  cache:
    # Cache per stage and branch.
    key: '$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG'
    paths:
      # Information on package dependency tree, package versions, frameworks etc.
      - '$SOURCES_DIR/$OBJECTS_DIR/project.assets.json'
      # NuGet and MSBuild related files.
      - '$SOURCES_DIR/$OBJECTS_DIR/*.csproj.nuget.*'
      # NuGet cache.
      - '$NUGET_PACKAGES_DIR'

dotnet-build:
  stage: build
  before_script:
    - dotnet restore
  script:
    - dotnet build --configuration "$BUILD_CONFIGURATION" --no-restore
  artifacts:
    paths:
      - '$SOURCES_DIR/$OUTPUTS_DIR'
      - '$SOURCES_DIR/$OBJECTS_DIR/$BUILD_CONFIGURATION'
    expire_in: 1 month

dotnet-pack:
  stage: pack
  needs:
    - job: dotnet-build
      artifacts: true
  before_script:
    - dotnet restore
  script:
    - dotnet pack --configuration "$BUILD_CONFIGURATION" --no-restore --no-build --include-symbols
  artifacts:
    paths:
      - '$SOURCES_DIR/$OUTPUTS_DIR/*/*.nupkg'
      - '$SOURCES_DIR/$OUTPUTS_DIR/*/*.snupkg'

.nuget-push: &nuget-push-common
  stage: deploy
  needs:
    - job: dotnet-pack
      artifacts: true
  # Do not use cache.
  inherit:
    default: false
  # Skip Git fetch.
  variables: &nuget-push-common-variables
    GIT_STRATEGY: none
  # Git not needed, use smaller Alpine image.
  image: mcr.microsoft.com/dotnet/sdk:6.0-alpine

# Push every package to GitLab package repository.
nuget-push:gitlab:
  <<: *nuget-push-common
  environment:
    name: NuGet GitLab Project
  before_script:
    - dotnet nuget add source "$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/nuget/index.json"
                              --name GitLab --username gitlab-ci-token --password "$CI_JOB_TOKEN" --store-password-in-clear-text
  script:
    - dotnet nuget push "$SOURCES_DIR/$OUTPUTS_DIR/*/*.nupkg" --source GitLab --skip-duplicate

# Push every package to staging environment.
nuget-push:staging:
  <<: *nuget-push-common
  environment:
    name: NuGet Staging
  script:
    - dotnet nuget push "$SOURCES_DIR/$OUTPUTS_DIR/*/*.nupkg"
                        --source "$NUGET_STAGING_SOURCE" --skip-duplicate --api-key "$NUGET_STAGING_API_KEY"

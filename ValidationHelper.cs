﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Microsoft.AspNetCore.Razor.TagHelpers;
using XploRe.Runtime;


namespace XploRe.AspNetCore.Mvc.TagHelpers.Bootstrap4
{

    /// <inheritdoc />
    /// <summary>
    /// <para>
    ///     Augments tags with bootstrap validation classes and messages if validation failed for the model field
    ///     identified by the expression in the attribute <c>bs-validation-for</c>.
    /// </para>
    /// <para>
    ///     For controls (input, textarea and select tags), the <c>form-control</c> and <c>form-control-danger</c>
    ///     classes are added.
    /// </para>
    /// <para>
    ///     If the tag has a <c>form-group</c> class set, the <c>has-danger</c> class is added. Otherwise, the
    ///     <c>form-control-feedback</c> class is added and the tag content is set to the corresponding validation error
    ///     message.
    /// </para>
    /// </summary>
    [HtmlTargetElement("*", Attributes = ValidationForAttributeName)]
    public class ValidationHelper : TagHelper
    {

        private const string ValidationForAttributeName = "bs-validation-for";


        #region Attributes

        /// <summary>
        ///     Name to be validated on the current model. If not specified, no action is performed.
        /// </summary>
        [CanBeNull]
        [HtmlAttributeName(ValidationForAttributeName)]
        public ModelExpression For { get; set; }

        #endregion


        /// <summary>
        ///     The automatically assigned <see cref="ViewContext" /> instance.
        /// </summary>
        [HtmlAttributeNotBound]
        [ViewContext]
        public ViewContext ViewContext { get; set; }

        /// <summary>
        ///     List of recognised control tags that will receive Bootstrap-specific classes.
        /// </summary>
        [NotNull]
        private static readonly List<string> ControlTagNames = new List<string> {
            "input",
            "select",
            "textarea"
        };

        /// <inheritdoc />
        /// <remarks>Does nothing if <see cref="For" /> is <c>null</c>.</remarks>
        public override async Task ProcessAsync([NotNull] TagHelperContext context, [NotNull] TagHelperOutput output)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (output == null) {
                throw new ArgumentNullException(nameof(output));
            }

            if (For == null) {
                return;
            }

            // Try to resolve For expression.
            var fullName = NameAndIdProvider.GetFullHtmlFieldName(ViewContext, For.Name);

            if (string.IsNullOrEmpty(fullName)) {
                throw new ArgumentException(
                    $"Field name given by '{ValidationForAttributeName}' cannot be null or empty"
                );
            }

            if (ViewContext == null) {
                throw new RuntimeInconsistencyException(
                    "{0}.{1} is null.".FormatWith(typeof(TagHelper), nameof(ViewContext))
                );
            }

            ModelStateEntry entry = null;
            ViewContext.ViewData?.ModelState?.TryGetValue(fullName, out entry);

            var hasError =
                null != entry && entry.Errors?.Any() == true;

            var tag = new TagBuilder(output.TagName);
            var appendMessage = true;

            // Decide validation error action depending on tag type.
            if (ControlTagNames.Contains(output.TagName)) {
                // A form control. Apply Bootstrap 4 control classes.
                tag.AddCssClass("form-control");

                if (hasError) {
                    tag.AddCssClass("is-invalid");
                }

                appendMessage = false;
            }
            else {
                // For all other tags, check for specific handling. Per default, a tag receives the validation error
                // message as content, unless appendMessage is set to false.
                var classes = output.Attributes
                                    ?.FirstOrDefault(attr => attr != null && attr.Name == "class")
                                    ?.Value
                                    ?.ToString()
                                    ?.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (classes != null) {
                    // Special handling for nodes with specific classes.
                    if (classes.Contains("form-group", StringComparer.OrdinalIgnoreCase)) {
                        // A form group, that may contain inputs and labels, that will inherit styling.
                        if (hasError) {
                            tag.AddCssClass("is-invalid");
                        }

                        appendMessage = false;
                    }
                }
            }

            output.MergeAttributes(tag);

            if (appendMessage) {
                // Append validation error message to tag body.
                var tagBuilder = Generator.GenerateValidationMessage(
                    ViewContext,
                    For.ModelExplorer,
                    For.Name,
                    message: null, tag: null, htmlAttributes: null
                );

                if (tagBuilder != null) {
                    // We assume the validation is related to an existing form input. Always merge tag attributes for
                    // client-side validation support.
                    tagBuilder.AddCssClass("form-control-feedback");

                    if (tagBuilder.Attributes != null) {
                        tagBuilder.Attributes["data-valmsg-for"] = fullName;
                        tagBuilder.Attributes["data-valmsg-replace"] = "true";
                    }

                    output.MergeAttributes(tagBuilder);

                    // Do not update the content if another tag helper targeting this element has already done so.
                    // Cf. https://github.com/aspnet/Mvc/blob/dev/src/Microsoft.AspNetCore.Mvc.TagHelpers/ValidationMessageTagHelper.cs
                    if (hasError && !output.IsContentModified) {
                        // We check for whitespace to detect scenarios such as:
                        // <span validation-for="Name">
                        // </span>
                        var childContentAsync = output.GetChildContentAsync();

                        if (childContentAsync != null) {
                            var childContent = await childContentAsync;

                            if (childContent?.IsEmptyOrWhiteSpace == true) {
                                // Provide default message text (if any) since there was nothing useful in the Razor 
                                // source.
                                if (tagBuilder.HasInnerHtml) {
                                    output.Content?.SetHtmlContent(tagBuilder.InnerHtml);
                                }
                            }
                            else {
                                output.Content?.SetHtmlContent(childContent);
                            }
                        }
                    }
                }
            }
        }


        #region (Services)

        /// <summary>
        ///     The cached <see cref="IHtmlGenerator" /> service instance.
        /// </summary>
        [NotNull]
        [HtmlAttributeNotBound]
        protected IHtmlGenerator Generator { get; }

        /// <summary>
        ///     Initialises a new <see cref="ValidationHelper" /> instance.
        /// </summary>
        /// <param name="generator">The <see cref="IHtmlGenerator" /> instance retrieved via DI.</param>
        public ValidationHelper([NotNull] IHtmlGenerator generator)
        {
            if (generator == null) {
                throw new ArgumentNullException(nameof(generator));
            }

            Generator = generator;
        }

        #endregion

    }

}

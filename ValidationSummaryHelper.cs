﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Microsoft.AspNetCore.Razor.TagHelpers;
using XploRe.Runtime;


namespace XploRe.AspNetCore.Mvc.TagHelpers.Bootstrap4
{

    /// <inheritdoc />
    /// <summary>
    ///     Transforms a given tag into a dismissable alert message with all corresponding errors listed in an unordered
    ///     list. Existing content is kept. If the model has no validation errors, output of the whole tag is completely
    ///     suppressed.
    /// </summary>
    [HtmlTargetElement("*", Attributes = ValidationSummaryAttributeName)]
    public class ValidationSummaryHelper : TagHelper
    {

        private const string FontAwesomeCloseIconAttributeName = "fa-close-icon";
        private const string ValidationSummaryAttributeName = "bs-validation-summary";


        #region Attributes

        /// <summary>
        ///     Uses the specified Font Awesome icon (without <c>fa-</c> prefix) instead of the default times ("x") icon.
        /// </summary>
        [CanBeNull]
        [HtmlAttributeName(FontAwesomeCloseIconAttributeName)]
        public string FontAwesomeCloseIcon { get; set; }

        /// <summary>
        ///     A validation summary is appended if either <see cref="Microsoft.AspNetCore.Mvc.Rendering.ValidationSummary.All" />
        ///     (includes properties) or <see cref="Microsoft.AspNetCore.Mvc.Rendering.ValidationSummary.ModelOnly" />
        ///     (excludes properties) is set.
        /// </summary>
        /// <exception cref="ArgumentException">
        ///     The provided <see cref="Microsoft.AspNetCore.Mvc.Rendering.ValidationSummary"/> value is unknown.
        /// </exception>
        [HtmlAttributeName(ValidationSummaryAttributeName)]
        public ValidationSummary ValidationSummary
        {
            get => _validationSummary;
            set {
                switch (value) {
                case ValidationSummary.All:
                case ValidationSummary.ModelOnly:
                case ValidationSummary.None:
                    _validationSummary = value;
                    break;

                default:
                    throw new ArgumentOutOfRangeException(
                        nameof(value), value,
                        $"Value is not a member of the {typeof(ValidationSummary)} enum."
                    );
                }
            }
        }

        private ValidationSummary _validationSummary;

        #endregion


        /// <summary>
        ///     The automatically assigned <see cref="ViewContext" /> instance.
        /// </summary>
        [HtmlAttributeNotBound]
        [ViewContext]
        public ViewContext ViewContext { get; set; }

        /// <inheritdoc />
        /// <remarks>
        ///     Output will be suppressed if <see cref="ValidationSummary"/> is set to
        ///     <see cref="Microsoft.AspNetCore.Mvc.Rendering.ValidationSummary.None"/>.
        /// </remarks>
        public override void Process([NotNull] TagHelperContext context, [NotNull] TagHelperOutput output)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (output == null) {
                throw new ArgumentNullException(nameof(output));
            }

            if (ValidationSummary == ValidationSummary.None) {
                // Won't encounter any errors, suppress output.
                output.SuppressOutput();
                return;
            }

            // All extra output is wrapped into a single UL that will be added to the container.
            var errorList = new TagBuilder("ul");
            var errorListEmpty = true;

            // The list is always appended to the end of the alert.
            errorList.AddCssClass("mb-0");

            // Check all errors and append list items accordingly.
            if (ViewContext == null) {
                throw new RuntimeInconsistencyException(
                    "{0}.{1} is null.".FormatWith(typeof(TagHelper), nameof(ViewContext))
                );
            }

            var modelStates = ValidationHelpers.GetModelStateList(
                ViewContext.ViewData,
                ValidationSummary == ValidationSummary.ModelOnly
            );

            if (modelStates == null) {
                throw new RuntimeInconsistencyException(
                    "{0}.{1}() returned null.".FormatWith(
                        typeof(ValidationHelpers),
                        nameof(ValidationHelpers.GetModelStateList)
                    )
                );
            }

            foreach (var modelState in modelStates) {
                if (modelState?.Errors == null) {
                    continue;
                }

                foreach (var modelError in modelState.Errors) {
                    var errorText = ValidationHelpers.GetModelErrorMessageOrDefault(modelError);

                    if (string.IsNullOrEmpty(errorText)) {
                        continue;
                    }

                    var listItem = new TagBuilder("li");
                    listItem.InnerHtml.SetContent(errorText);

                    errorList.InnerHtml.AppendLine(listItem);
                    errorListEmpty = false;
                }
            }

            if (errorListEmpty) {
                // No errors encountered, suppress output.
                output.SuppressOutput();
                return;
            }

            // Build close button.
            TagBuilder closeButtonLabel;

            if (!string.IsNullOrWhiteSpace(FontAwesomeCloseIcon)) {
                closeButtonLabel = new TagBuilder("i");
                closeButtonLabel.AddCssClass("fa");
                // Render without fixed width to avoid extra horizontal spacing.
                //-closeButtonLabel.AddCssClass("fa-fw");
                closeButtonLabel.AddCssClass("fa-" + FontAwesomeCloseIcon);
            }
            else {
                closeButtonLabel = new TagBuilder("span");
                closeButtonLabel.InnerHtml.SetHtmlContent("&times;");
            }

            closeButtonLabel.MergeAttribute("aria-hidden", "true");

            var closeButton = new TagBuilder("button");
            closeButton.MergeAttributes(
                new Dictionary<string, string> {
                    { "type", "button" },
                    { "class", "close" },
                    { "data-dismiss", "alert" },
                    { "aria-label", "Close" }
                });

            if (!string.IsNullOrWhiteSpace(FontAwesomeCloseIcon)) {
                // Fix padding as icon otherwise seems out-of-place.
                closeButton.AddCssClass("p-2");
            }

            closeButton.InnerHtml.AppendLine(closeButtonLabel);

            // Build alert.
            var tagBuilder = new TagBuilder(output.TagName);
            tagBuilder.AddCssClass("alert alert-danger alert-dismissible fade show");
            tagBuilder.MergeAttribute("role", "alert");

            output.MergeAttributes(tagBuilder);
            output.PreContent?.SetHtmlContent(closeButton);
            output.PostContent?.SetHtmlContent(errorList);
        }


        #region (Services)

        /// <summary>
        ///     The cached <see cref="IHtmlGenerator" /> service instance.
        /// </summary>
        [HtmlAttributeNotBound]
        protected IHtmlGenerator Generator { get; }

        /// <summary>
        ///     Initialises a new <see cref="ValidationSummaryHelper" /> instance.
        /// </summary>
        /// <param name="generator">The <see cref="IHtmlGenerator" /> instance retrieved via DI.</param>
        public ValidationSummaryHelper(IHtmlGenerator generator)
        {
            Generator = generator;
        }

        #endregion

    }

}
